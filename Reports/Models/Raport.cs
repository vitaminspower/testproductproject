﻿using System;

namespace Reports.Models
{
    public class Raport
    {
        public int Id { get; set; }
        public string ProductName { get; set; }
        public string BuyerName { get; set; }
        public DateTime Date { get; set; }
        public string ShopName { get; set; }
    }
}
