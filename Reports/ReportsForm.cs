﻿using Reports.Services;
using System;
using System.Windows.Forms;

namespace Reports
{
    public partial class ReportsForm : Form
    {
        private readonly DataService _dataService;

        public ReportsForm()
        {
            _dataService = new DataService();

            InitializeComponent();
            
            InitializeComboBox();
            RefreshDataGrid();
            RefreshSearchFilters();
        }

        private void ApplyFilersButton_Click(object sender, EventArgs e)
        {
            if (PlaceFilterComboBox.SelectedItem is null || PlaceFilterComboBox.SelectedItem is "search all locals")
            {
                DataGrid.DataSource = _dataService.GetDataFromDb(
                    DateTimeFilterBoxFrom.Value,
                    DateTimeFilterBoxBefore.Value);
            }
            else
            {
                DataGrid.DataSource = _dataService.GetDataFromDb(
                    DateTimeFilterBoxFrom.Value,
                    DateTimeFilterBoxBefore.Value,
                    PlaceFilterComboBox.SelectedItem.ToString());
            }

            DataGrid.Refresh();
        }

        private void InitializeComboBox()
        {
            PlaceFilterComboBox.Items.AddRange(new[]
            {
                "search all locals",
                "local1",
                "local2",
                "local3",
                "local4",
                "local5",
            });
        }

        private void RefreshDataGrid()
        {
            DataGrid.DataSource = _dataService.GetDataFromDb();
            DataGrid.Refresh();
        }

        private void RefreshSearchFilters()
        {
            PlaceFilterComboBox.Refresh();
            DateTimeFilterBoxBefore.Value = DateTime.UtcNow.AddDays(1);
            DateTimeFilterBoxFrom.Value = DateTime.UtcNow.AddDays(-1);
        }

        private void ShowAllButton_Click(object sender, EventArgs e)
        {
            RefreshSearchFilters();
            RefreshDataGrid();
        }

        private void DateTimeFilterBoxFrom_ValueChanged(object sender, EventArgs e)
        {
            if (DateTimeFilterBoxFrom.Value > DateTimeFilterBoxBefore.Value)
            {
                DateTimeFilterBoxFrom.Value = DateTime.UtcNow.AddDays(-1);
                MessageBox.Show("lower date cant be later then upper date");
            }
        }

        private void DateTimeFilterBoxBefore_ValueChanged(object sender, EventArgs e)
        {
            if (DateTimeFilterBoxBefore.Value < DateTimeFilterBoxFrom.Value)
            {
                DateTimeFilterBoxBefore.Value = DateTime.UtcNow.AddDays(1);
                MessageBox.Show("upper date cant be earlier then lower date");
            }
        }
    }
}
