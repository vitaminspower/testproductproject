﻿namespace Reports
{
    partial class ReportsForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.DataGrid = new System.Windows.Forms.DataGridView();
            this.PlaceFilterComboBox = new System.Windows.Forms.ComboBox();
            this.DateTimeFilterBoxFrom = new System.Windows.Forms.DateTimePicker();
            this.DateTimeFilterBoxBefore = new System.Windows.Forms.DateTimePicker();
            this.ApplyFilersBotton = new System.Windows.Forms.Button();
            this.ShowAllButton = new System.Windows.Forms.Button();
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            this.infoLabelPlace = new System.Windows.Forms.Label();
            this.infoLabelDateFrom = new System.Windows.Forms.Label();
            this.infoLabelDateBefore = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DataGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // DataGrid
            // 
            this.DataGrid.AllowUserToAddRows = false;
            this.DataGrid.AllowUserToDeleteRows = false;
            this.DataGrid.AllowUserToResizeColumns = false;
            this.DataGrid.AllowUserToResizeRows = false;
            this.DataGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this.DataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGrid.Location = new System.Drawing.Point(296, 12);
            this.DataGrid.Name = "DataGrid";
            this.DataGrid.ReadOnly = true;
            this.DataGrid.Size = new System.Drawing.Size(544, 338);
            this.DataGrid.TabIndex = 0;
            // 
            // PlaceFilterComboBox
            // 
            this.PlaceFilterComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.PlaceFilterComboBox.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.PlaceFilterComboBox.Location = new System.Drawing.Point(65, 12);
            this.PlaceFilterComboBox.Name = "PlaceFilterComboBox";
            this.PlaceFilterComboBox.Size = new System.Drawing.Size(226, 21);
            this.PlaceFilterComboBox.TabIndex = 1;
            // 
            // DateTimeFilterBoxFrom
            // 
            this.DateTimeFilterBoxFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DateTimeFilterBoxFrom.Location = new System.Drawing.Point(65, 55);
            this.DateTimeFilterBoxFrom.Name = "DateTimeFilterBoxFrom";
            this.DateTimeFilterBoxFrom.Size = new System.Drawing.Size(225, 20);
            this.DateTimeFilterBoxFrom.TabIndex = 2;
            this.DateTimeFilterBoxFrom.ValueChanged += new System.EventHandler(this.DateTimeFilterBoxFrom_ValueChanged);
            // 
            // DateTimeFilterBoxBefore
            // 
            this.DateTimeFilterBoxBefore.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DateTimeFilterBoxBefore.Location = new System.Drawing.Point(65, 81);
            this.DateTimeFilterBoxBefore.Name = "DateTimeFilterBoxBefore";
            this.DateTimeFilterBoxBefore.Size = new System.Drawing.Size(225, 20);
            this.DateTimeFilterBoxBefore.TabIndex = 3;
            this.DateTimeFilterBoxBefore.ValueChanged += new System.EventHandler(this.DateTimeFilterBoxBefore_ValueChanged);
            // 
            // ApplyFilersBotton
            // 
            this.ApplyFilersBotton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.25F);
            this.ApplyFilersBotton.Location = new System.Drawing.Point(13, 405);
            this.ApplyFilersBotton.Name = "ApplyFilersBotton";
            this.ApplyFilersBotton.Size = new System.Drawing.Size(127, 33);
            this.ApplyFilersBotton.TabIndex = 4;
            this.ApplyFilersBotton.Text = "Apply filters";
            this.ApplyFilersBotton.UseVisualStyleBackColor = true;
            this.ApplyFilersBotton.Click += new System.EventHandler(this.ApplyFilersButton_Click);
            // 
            // ShowAllButton
            // 
            this.ShowAllButton.AccessibleRole = System.Windows.Forms.AccessibleRole.ScrollBar;
            this.ShowAllButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.25F);
            this.ShowAllButton.Location = new System.Drawing.Point(713, 405);
            this.ShowAllButton.Name = "ShowAllButton";
            this.ShowAllButton.Size = new System.Drawing.Size(127, 33);
            this.ShowAllButton.TabIndex = 5;
            this.ShowAllButton.Text = "Refresh";
            this.ShowAllButton.UseVisualStyleBackColor = true;
            this.ShowAllButton.Click += new System.EventHandler(this.ShowAllButton_Click);
            // 
            // infoLabelPlace
            // 
            this.infoLabelPlace.AutoSize = true;
            this.infoLabelPlace.Location = new System.Drawing.Point(10, 15);
            this.infoLabelPlace.Name = "infoLabelPlace";
            this.infoLabelPlace.Size = new System.Drawing.Size(32, 13);
            this.infoLabelPlace.TabIndex = 6;
            this.infoLabelPlace.Text = "Shop";
            // 
            // infoLabelDateFrom
            // 
            this.infoLabelDateFrom.AutoSize = true;
            this.infoLabelDateFrom.Location = new System.Drawing.Point(10, 55);
            this.infoLabelDateFrom.Name = "infoLabelDateFrom";
            this.infoLabelDateFrom.Size = new System.Drawing.Size(30, 13);
            this.infoLabelDateFrom.TabIndex = 7;
            this.infoLabelDateFrom.Text = "From";
            // 
            // infoLabelDateBefore
            // 
            this.infoLabelDateBefore.AutoSize = true;
            this.infoLabelDateBefore.Location = new System.Drawing.Point(10, 81);
            this.infoLabelDateBefore.Name = "infoLabelDateBefore";
            this.infoLabelDateBefore.Size = new System.Drawing.Size(38, 13);
            this.infoLabelDateBefore.TabIndex = 8;
            this.infoLabelDateBefore.Text = "Before";
            // 
            // ReportsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(873, 457);
            this.Controls.Add(this.infoLabelDateBefore);
            this.Controls.Add(this.infoLabelDateFrom);
            this.Controls.Add(this.infoLabelPlace);
            this.Controls.Add(this.ShowAllButton);
            this.Controls.Add(this.ApplyFilersBotton);
            this.Controls.Add(this.DateTimeFilterBoxBefore);
            this.Controls.Add(this.DateTimeFilterBoxFrom);
            this.Controls.Add(this.PlaceFilterComboBox);
            this.Controls.Add(this.DataGrid);
            this.Name = "ReportsForm";
            this.Text = "Reports";
            ((System.ComponentModel.ISupportInitialize)(this.DataGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView DataGrid;
        private System.Windows.Forms.ComboBox PlaceFilterComboBox;
        private System.Windows.Forms.DateTimePicker DateTimeFilterBoxFrom;
        private System.Windows.Forms.DateTimePicker DateTimeFilterBoxBefore;
        private System.Windows.Forms.Button ApplyFilersBotton;
        private System.Windows.Forms.Button ShowAllButton;
        private System.Windows.Forms.HelpProvider helpProvider1;
        private System.Windows.Forms.Label infoLabelPlace;
        private System.Windows.Forms.Label infoLabelDateFrom;
        private System.Windows.Forms.Label infoLabelDateBefore;
    }
}

