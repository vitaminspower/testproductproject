﻿using System.Data.Entity;
using Reports.Models;

namespace Reports.Data
{
    public class MainDbContext : DbContext
    {
        public MainDbContext() : base("DefaultConnection")
        {
        }

        public DbSet<Raport> Raports { get; set; }
    }
}
