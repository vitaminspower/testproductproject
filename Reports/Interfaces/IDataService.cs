﻿using System;
using System.Data;

namespace Reports.Interfaces
{
    public interface IDataService
    {
        DataTable GetDataFromDb();

        DataTable GetDataFromDb(DateTime timeBorderFrom, DateTime timeBorderTo);

        DataTable GetDataFromDb(DateTime timeBorderFrom, DateTime timeBorderTo, string shopName);
    }
}