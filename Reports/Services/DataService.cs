﻿using System;
using System.Data;
using System.Linq;
using Reports.Data;
using Reports.Interfaces;

namespace Reports.Services
{
    public class DataService : IDataService
    {
        public DataTable GetDataFromDb()
        {
            var table = new DataTable();

            using (var db = new MainDbContext())
            {
                table = InitializeColumns(table);

                foreach (var item in db.Raports)
                {
                    table.Rows.Add(
                        item.ProductName,
                        item.Date.Date,
                        $"{item.Date.Hour}:{item.Date.Minute}",
                        item.BuyerName,
                        item.ShopName);
                }
            }

            return table;
        }

        public DataTable GetDataFromDb(DateTime timeBorderFrom, DateTime timeBorderTo)
        {
            var table = new DataTable();

            using (var db = new MainDbContext())
            {
                table = InitializeColumns(table);

                foreach (var item in db.Raports
                             .Where(x =>
                                 x.Date >= timeBorderFrom
                                 && x.Date <= timeBorderTo))
                {
                    table.Rows.Add(
                        item.ProductName,
                        item.Date.Date,
                        $"{item.Date.Hour}:{item.Date.Minute}",
                        item.BuyerName,
                        item.ShopName);
                }
            }

            return table;
        }

        public DataTable GetDataFromDb(DateTime timeBorderFrom, DateTime timeBorderTo, string shopName)
        {
            var table = new DataTable();

            using (var db = new MainDbContext())
            {
                table = InitializeColumns(table);

                foreach (var item in db.Raports
                             .Where(x => 
                                 x.Date >= timeBorderFrom 
                                 && x.Date <= timeBorderTo 
                                 && x.ShopName == shopName))
                {
                    table.Rows.Add(
                        item.ProductName,
                        item.Date.Date,
                        $"{item.Date.Hour}:{item.Date.Minute}",
                        item.BuyerName,
                        item.ShopName);
                }
            }

            return table;
        }

        private DataTable InitializeColumns(DataTable table)
        {
            table.Columns.Add("Product", typeof(string));
            table.Columns.Add("Date", typeof(DateTime));
            table.Columns.Add("Time", typeof(string));
            table.Columns.Add("Customer", typeof(string));
            table.Columns.Add("Shop", typeof(string));

            return table;
        }
    }
}

